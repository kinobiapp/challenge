Questionnaire answers are in Questionnaire.README.md

Software requirements:

* Install node v6.9.5 via nvm
* The code has been written in Atom with jshint and 'esversion: 6' enabled

Installation instructions:

* git clone [url of this repo]
* npm install -g yarnpkg
* Run unit tests: npm test
* Run the server: npm run server
* Run acceptance tests (server must be running, use another window): npm run acceptance-tests (restart server after each test run to repopulate test data in the database)
* Use postman or curl to access the API

Assumptions made about the requirements of the task:

* The application has been built as a REST API with no front end. API will be driven by the tests or using curl/postman
* Members and rewards must be retrieved separately to avoid a possibly unbounded set of rewards included in members result, but the link to rewards for a member is included as a HATEOS link.
* Duplicate member names are permitted

Anything you believe is important but out of scope or unnecessary:

* Authentication on the endpoints
* Any authorization on entities or multi-tenancy checks, i.e. who should be allowed to access/delete what
* Logging
* Caching of previously retrieved resources
* Paging of data for performance reasons
* More validation
* Due to lack of time I didn't complete extensive unit testing, however I have added one example unit test to the solution
