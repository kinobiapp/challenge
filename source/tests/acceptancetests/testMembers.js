const config = require('./config');

const {
    expect,
    post,
    get,
    del
} = require('chakram');

const {
    assert,
    should
} = require('chai');

const httpstatus = require('http-status-codes');

// initialize chai should;
should();

const Member = require('../../model/Member.js');
const Reward = require('../../model/Reward.js');

describe('/members', () => {
    describe('POST /members', () => {
        it('POST /members with valid payload should return 201 Created', () => {
            let newMember = new Member(null, 'Test Member');
            return post(`${config.baseUrl}/members`, newMember).then(response => {
                expect(response).to.have.status(httpstatus.CREATED);
            });
        });

        it('POST /members with invalid payload should return 400 Bad Request', () => {
            let newMember = {
                someProperty: 'value'
            };
            return post(`${config.baseUrl}/members`, newMember).then(response => {
                expect(response).to.have.status(httpstatus.BAD_REQUEST);
            });
        });

        it('POST /members with no data should return 400 Bad Request', () => {
            let newMember = null;
            return post(`${config.baseUrl}/members`, newMember).then(response => {
                expect(response).to.have.status(httpstatus.BAD_REQUEST);
            });
        });

        it('POST /members with a valid payload should return the created resource', () => {
            let newMember = new Member(null, 'Test Member');
            return post(`${config.baseUrl}/members`, newMember).then(response => {
                expect(response.body.data).to.have.property("id");
            });
        });
    });

    describe('GET /members', () => {
        it('GET /members/:memberId with an known id should return the 200 Success', () => {
            return get(`${config.baseUrl}/members`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.OK);
                });
        });

        it('GET /members/:memberId with an unknown id should return 404 Not Found', () => {
            let memberId = -99;
            return get(`${config.baseUrl}/members/${memberId}`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NOT_FOUND);
                });
        });

        it('GET /members/:memberId with a known id should return the requested resource', () => {
            let memberId = 1;
            return get(`${config.baseUrl}/members/${memberId}`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.OK);
                });
        });

        it('GET /members/:memberId returns a resource with an embedded link to rewards', () => {
            let memberId = 1;
            return get(`${config.baseUrl}/members/${memberId}`)
                .then(response => {
                    expect(response.body.data.rewards).to.equal(`/members/${memberId}/rewards`);
                });
        });
    });

    describe('DELETE /members', () => {
        it('DELETE /members/:memberId with a known id should return 204 No Content', () => {
            let memberId = 3;
            return del(`${config.baseUrl}/members/${memberId}`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NO_CONTENT);
                });
        });

        it('DELETE /members/:memberId with an unknown id should return 404 Not Found', () => {
            let memberId = -1;
            return del(`${config.baseUrl}/members/${memberId}`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NOT_FOUND);
                });
        });

        it('DELETE /members/:memberId with a known id should delete the member and rewards', () => {
            let memberId = 4;
            return del(`${config.baseUrl}/members/${memberId}`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NO_CONTENT);
                }).then(() => {
                    return get(`${config.baseUrl}/members/${memberId}`);
                }).then(response => {
                    expect(response).to.have.status(httpstatus.NOT_FOUND);
                }).then(() => {
                    return get(`${config.baseUrl}/members/${memberId}/rewards`);
                }).then((response) => {
                    expect(response).to.have.status(httpstatus.NOT_FOUND);
                });
        });
    });
});

describe('/members/:memberId/rewards', () => {
    describe('POST /members/:memberId/rewards', () => {
        it('POST /members/:memberId/rewards with a valid payload returns 201 Created', () => {
            let memberId = 1;
            let reward = new Reward(null, memberId, "Test Reward");
            return post(`${config.baseUrl}/members/${memberId}/rewards`, reward)
                .then(response => {
                    expect(response).to.have.status(httpstatus.CREATED);
                });
        });

        it('POST /members/:memberId/rewards with an unknown member id should return 404 Not Found', () => {
            let memberId = -1;
            let reward = new Reward(null, memberId, "Test Reward");
            return post(`${config.baseUrl}/members/${memberId}/rewards`, reward)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NOT_FOUND);
                });
        });

        it('POST /members/:memberId/rewards with an invalid payload returns 400 Bad Request', () => {
            let memberId = 1;
            return post(`${config.baseUrl}/members/${memberId}/rewards`, {})
                .then(response => {
                    expect(response).to.have.status(httpstatus.BAD_REQUEST);
                });
        });

        it('POST /members/:memberId/rewards with an empty payload returns 400 Bad Request', () => {
            let memberId = 1;
            return post(`${config.baseUrl}/members/${memberId}/rewards`, null)
                .then(response => {
                    expect(response).to.have.status(httpstatus.BAD_REQUEST);
                });
        });

        it('POST /members/:memberId/rewards adds the reward to the member', () => {
            let memberId = 5;
            let reward = new Reward(null, memberId, "Test Reward");
            return post(`${config.baseUrl}/members/${memberId}/rewards`, reward)
                .then(response => {
                    expect(response).to.have.status(httpstatus.CREATED);
                }).then(() => {
                    return get(`${config.baseUrl}/members/${memberId}/rewards`);
                }).then(response => {
                    expect(response).to.have.status(httpstatus.OK);
                    expect(response.body.data.length).to.equal(1);
                    expect(response.body.data[0].memberId).to.equal(memberId);
                    expect(response.body.data[0].rewardType).to.equal(reward.rewardType);
                });
        });

        it('POST /members/:memberId/rewards should return the created resource', () => {
            let memberId = 5;
            let reward = new Reward(null, memberId, "Test Reward");
            return post(`${config.baseUrl}/members/${memberId}/rewards`, reward)
                .then(response => {
                    expect(response.body.data.memberId).to.equal(memberId);
                    expect(response.body.data.rewardType).to.equal(reward.rewardType);
                });
        });
    });

    describe('GET /members/:memberId/rewards', () => {
        it('GET /members/:memberId/rewards with a known member id returns 200 Success', () => {
            let memberId = 1;
            get(`${config.baseUrl}/members/${memberId}/rewards`).then(response => {
                expect(response).to.have.status(httpstatus.OK);
            });
        });

        it('GET /members/:memberId/rewards with a known member id returns the members rewards', () => {
            let memberId = 1;
            return get(`${config.baseUrl}/members/${memberId}/rewards`).then(response => {
                expect(response.body.data.length).to.equal(3);
            });
        });

        it('GET /members/:memberId/rewards with an unknown member id returns 404 Not Found', () => {
            let memberId = -1;
            return get(`${config.baseUrl}/members/${memberId}/rewards`).then(response => {
                expect(response).to.have.status(httpstatus.NOT_FOUND);
            });
        });

        it('GET /members/:memberId/rewards/:rewardId with a valid reward id returns 200 Success', () => {
            let memberId = 1;
            let rewardId = 1;
            return get(`${config.baseUrl}/members/${memberId}/rewards/${rewardId}`).then(response => {
                expect(response).to.have.status(httpstatus.OK);
            });
        });

        it('GET /members/:memberId/rewards/:rewardId with a known reward id returns the requested resource', () => {
            let memberId = 1;
            let rewardId = 1;
            return get(`${config.baseUrl}/members/${memberId}/rewards/${rewardId}`).then(response => {
                expect(response.body.data.id).to.equal(rewardId);
            });
        });

        it('GET /members/:memberId/rewards/:rewardId with an unknown reward id return 404 Not Found', () => {
            let memberId = 1;
            let rewardId = -1;
            return get(`${config.baseUrl}/members/${memberId}/rewards/${rewardId}`).then(response => {
                expect(response).to.have.status(httpstatus.NOT_FOUND);
            });
        });

    });
    describe('DELETE /members/:memberId/rewards', () => {

        it('DELETE /members/:memberId/rewards should return 204 No Content', () => {
            let memberId = 6;
            return del(`${config.baseUrl}/members/${memberId}/rewards`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NO_CONTENT);
                });

        });

        it('DELETE /members/:memberId/rewards should delete the requested resources', () => {
            let memberId = 7;

            return del(`${config.baseUrl}/members/${memberId}/rewards`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NO_CONTENT);
                });
        });

        it('DELETE /members/:memberId/rewards with an unknown memberId should return 404 Not Found', () => {
            let memberId = -1;

            return del(`${config.baseUrl}/members/${memberId}/rewards`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NOT_FOUND);
                }).then(() => {
                    return get(`${config.baseUrl}/members/${memberId}/rewards`);
                }).then(response => {
                    expect(response).to.have.status(httpstatus.NOT_FOUND);
                });
        });

        it('DELETE /members/:memberId/rewards/:rewardId with a known rewardId should return 204 No Content', () => {
            let memberId = 8;
            let rewardId = 8;
            return del(`${config.baseUrl}/members/${memberId}/rewards/${rewardId}`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NO_CONTENT);
                });
        });

        it('DELETE /members/:memberId/rewards/:rewardId with a known rewardId should delete the requested resource', () => {
            let memberId = 8;
            let rewardId = 9;
            return del(`${config.baseUrl}/members/${memberId}/rewards/${rewardId}`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NO_CONTENT);
                })
                .then(() => {
                    return get(`${config.baseUrl}/members/${memberId}/rewards/${rewardId}`);
                }).then(response => {
                    expect(response).to.have.status(httpstatus.NOT_FOUND);
                });
        });

        it('DELETE /members/:memberId/rewards/:rewardId with an unknown rewardId should return 400 Bad Request', () => {
            let memberId = 8;
            let rewardId = 10;
            return del(`${config.baseUrl}/members/${memberId}/rewards/${rewardId}`)
                .then(response => {
                    expect(response).to.have.status(httpstatus.NOT_FOUND);
                });
        });
    });
});
