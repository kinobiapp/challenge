const sinon = require("sinon");
const sinonPromise = require("sinon-promise");
const {
    expect
} = require("chai");
const resources = require("../../model/resources");
const Member = require("../../model/Member");

sinonPromise(sinon);

let target;
let repo;
let sandbox;
let req;
let next;

describe('Members middlware', () => {
    beforeEach(() => {
        target = require("../../middleware/Members");
        repo = require("../../model/MembersRepo");

        req = {
            params: {},
            locals: {}
        };

        next = sinon.spy();

        sandbox = sinon.sandbox.create();
    });

    afterEach(() => sandbox.restore());

    it('should set req.locals.data when a member is found', () => {
        let member = new Member(1, "Test Member");

        sandbox.stub(repo, 'readMember', sinon.promise().resolves(member));

        req.params.memberId = member.id;

        target
            .getMember(req)
            .then(() => {
                expect(req.locals.data).to.equal(member);
            });
    });

    it('should set req.locals.notFound when a member is not found', () => {

        let memberId = -1;
        let err = new Error(resources.errors.UNKNOWN_MEMBER);

        sandbox.stub(repo, 'readMember', sinon.promise().rejects(err));

        req.params.memberId = memberId;

        target
            .getMember(req)
            .then(() => {
                expect(req.locals.data).to.be.null();
                expect(req.locals.notFound).to.be.true();
            });
    });
});
