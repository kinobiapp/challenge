module.exports = {
    port: process.env.PORT || 8000,
    serverName: "doshii challenge members service",
    serverVersion: "1.0.0"
};
