const repo = require('../model/RewardsRepo');
const resources = require('../model/resources');

class Rewards {

    static postReward(req) {
        return repo.createReward(req.params.memberId, req.body)
            .then(reward => {
                req.locals.data = reward;
                req.locals.newdata = true;
            });
    }

    static getRewards(req) {
        return repo.getRewards(req.params.memberId).then(rewards => {
            if (!rewards) {
                req.locals.data = null;
                req.locals.notFound = true;
            } else
                req.locals.data = rewards;
        });
    }

    static getReward(req) {
        return repo.getReward(req.params.memberId, req.params.rewardId)
            .then(reward => {
                if (!reward) {
                    req.locals.data = null;
                    req.locals.notFound = true;
                } else
                    req.locals.data = reward;
            });
    }

    static deleteRewards(req) {
        return repo.deleteRewards(req.params.memberId)
            .then(() => {
                req.locals.data = null;
                req.locals.deleted = true;
            }).catch(err => {
                if (err.message === resources.errors.UNKNOWN_REWARD) {
                    req.locals.notFound = true;
                } else {
                    throw err;
                }
            });
    }

    static deleteReward(req) {
        return repo.deleteReward(req.params.memberId, req.params.rewardId)
            .then(() => {
                req.locals.data = null;
                req.locals.deleted = true;
            })
            .catch(err => {
                if (err.message === resources.errors.UNKNOWN_REWARD) {
                    req.locals.notFound = true;
                } else {
                    throw err;
                }
            });
    }
}

module.exports = Rewards;
