const repo = require('../model/MembersRepo');
const resources = require('../model/resources');

class Members {
    static getMembers(req) {
        return repo
            .readMembers()
            .then(members => {
                if (members === null || members.length === 0) {
                    req.locals.notFound = true;
                }
                req.locals.data = members;
            });
    }

    static getMember(req) {
        return repo
            .readMember(req.params.memberId)
            .then(member => {
                if (member === null) {
                    req.locals.notFound = true;
                }
                req.locals.data = member;
            });
    }

    static postMember(req) {
        return repo.createMember(req.body)
            .then(member => {
                req.locals.data = member;
                req.locals.newdata = true;
            });
    }

    static deleteMember(req) {
        return repo.deleteMember(req.params.memberId)
            .then(() => {
                req.locals.data = null;
                req.locals.deleted = true;
            })
            .catch(err => {
                if (err.message === resources.errors.UNKNOWN_MEMBER) {
                    req.locals.notFound = true;
                } else {
                    throw err;
                }
            });
    }
}

module.exports = Members;
