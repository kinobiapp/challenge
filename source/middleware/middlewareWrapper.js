module.exports = {
    // wrap a middleware function and ensure errors are handled correctly
    // and that next is called on success
    wrap: (fn) => (req, res, next) => {
        fn(req)
            .then(() => next())
            .catch(err => {
                req.locals.error = err;
                req.log.error(err);
                next();
            });
    }
};
