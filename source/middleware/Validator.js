const {
    errors
} = require('restify');
const httpstatus = require('http-status-codes');
const Response = require('../model/Response');

// validate incoming data
module.exports = {

    validateMember: (req, res, next) => {
        if (!req.body || !req.body.name) {
            res.status(httpstatus.BAD_REQUEST);
            let response = new Response(httpstatus.BAD_REQUEST, 'validation failed');
            return res.json(response);
        }

        return next();
    },

    validateReward: (req, res, next) => {
        if (!req.body || !req.body.rewardType || !req.body.memberId || req.body.memberId === 0) {
            res.status(httpstatus.BAD_REQUEST);
            let response = new Response(httpstatus.BAD_REQUEST, 'validation failed');
            return res.json(response);
        }

        return next();
    }
};
