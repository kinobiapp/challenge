const createLink = (orig, obj) => {
    // use a regular expression to replace ids in the link string
    // with properties from the object
    var rx = /\:([a-zA-Z]+)[\/\&\.]*/gi;
    out = "";
    while ((matches = rx.exec(orig)) !== null) {
        out = orig.replace(`:${matches[1]}`, obj[matches[1]]);
    }
    return out;
};

module.exports = {
    // create links to sub resources in the payload
    link: (prop, link) => (req, res, next) => {

        if (!req.locals || !req.locals.data || req.locals.error) return next();

        data = req.locals.data;

        if (data instanceof Array) {
            req.locals.data.forEach(i => {
                i[prop] = createLink(link, i);
            });
        } else {
            data[prop] = createLink(link, data);
        }

        return next();
    }
};
