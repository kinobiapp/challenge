const status = require('http-status-codes');
const Response = require('../model/Response');
const {
    errors
} = require('restify');

module.exports = class Responder {
    // send a response to the client based on the request.locals state
    static respond(req, res, next) {

        if (req.locals.deleted) {
            return Responder.sendDeleted(req, res);
        }

        if (req.locals.notFound) {
            return Responder.sendNotFound(req, res);
        }

        if (req.locals.newdata) {
            return Responder.sendCreated(req, res, req.locals.data);
        }

        if (!req.locals.data || req.locals.data.length === 0) {
            return Responder.sendNotFound(req, res);
        }

        return Responder.sendSuccess(req, res, req.locals.data);
    }

    static sendSuccess(req, res, data) {
        res.status(status.OK);
        let response = new Response(status.OK, data);
        res.json(response);
    }

    static sendNotFound(req, res) {
        res.status(status.NOT_FOUND);
        let response = new Response(status.NOT_FOUND, null);
        res.json(response);
    }

    static sendCreated(req, res, data) {
        res.status(status.CREATED);
        let response = new Response(status.CREATED, data);
        res.json(response);
    }

    static sendDeleted(req, res) {
        res.status(status.NO_CONTENT);
        let response = new Response(status.NO_CONTENT, null);
        res.json(response);
    }
};
