const {
    createServer,
    queryParser,
    bodyParser,
    requestLogger,
    auditLogger
} = require("restify");

const {
    createLogger
} = require("bunyan");

const config = require("./config");
const membersMiddleware = require("./middleware/Members");
const rewardsMiddleware = require("./middleware/Rewards");
const linker = require("./middleware/Linker");
const responder = require("./middleware/Responder");
const validator = require("./middleware/Validator");
const database = require("./model/database");
const wrapper = require("./middleware/middlewareWrapper");

// set up logging
const logger = createLogger({
    name: config.serverName
});

var server = createServer({
    name: config.serverName,
    version: config.serverVersion,
    log: logger
});

server.use(queryParser());
server.use(bodyParser());
server.use(requestLogger());

// setup a storage area for local data to be passed between middlewares
server.pre((req, res, next) => {
    req.locals = {};
    next();
});

server.on('after', auditLogger({
    log: logger
}));

// setup routes for members
server.get("/members",
    wrapper.wrap(membersMiddleware.getMembers),
    linker.link("rewards", "/members/:id/rewards"),
    responder.respond);

server.get("/members/:memberId",
    wrapper.wrap(membersMiddleware.getMember),
    linker.link("rewards", "/members/:id/rewards"),
    responder.respond);

server.post("/members",
    validator.validateMember,
    wrapper.wrap(membersMiddleware.postMember),
    linker.link("rewards", "/members/:id/rewards"),
    responder.respond);

server.del("/members/:memberId",
    wrapper.wrap(membersMiddleware.deleteMember),
    responder.respond);

// setup routes for rewards
server.post("/members/:memberId/rewards",
    validator.validateReward,
    wrapper.wrap(rewardsMiddleware.postReward),
    responder.respond
);

server.get("/members/:memberId/rewards",
    wrapper.wrap(rewardsMiddleware.getRewards),
    responder.respond
);

server.get("/members/:memberId/rewards/:rewardId",
    wrapper.wrap(rewardsMiddleware.getReward),
    responder.respond
);

server.del("/members/:memberId/rewards",
    wrapper.wrap(rewardsMiddleware.deleteRewards),
    responder.respond);

server.del("/members/:memberId/rewards/:rewardId",
    wrapper.wrap(rewardsMiddleware.deleteReward),
    responder.respond);

database
    .connect()
    .then(() => {
        // database is setup, start listening
        server.listen(config.port, () => {
            server.log.info(`server listening on port ${config.port}`);
        });
    })
    .catch(err => {
        server.log.error(err.stack);
        server.close(function() {
            process.exit(0);
        });
    });

process.on("SIGTERM", function() {
    server.log.info("server closing");
    server.close(function() {
        process.exit(0);
    });
});
