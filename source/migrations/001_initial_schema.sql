--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE Member (id INTEGER PRIMARY KEY, name TEXT);
CREATE TABLE Reward (id INTEGER PRIMARY KEY, memberId INTEGER,  rewardType TEXT,
  CONSTRAINT FK_Reward_Member FOREIGN KEY (memberId)
    REFERENCES Member (id) ON UPDATE CASCADE ON DELETE CASCADE);
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP TABLE Member;
DROP TABLE Reward;
