-- Up
INSERT INTO Member (id, name) VALUES (1, 'First Member');
INSERT INTO Member (id, name) VALUES (2, 'Second Member');
INSERT INTO Member (id, name) VALUES (3, 'Member to Delete');
INSERT INTO Member (id, name) VALUES (4, 'Member to Delete');
INSERT INTO Member (id, name) VALUES (5, 'Rewarded Member 1');
INSERT INTO Member (id, name) VALUES (6, 'Rewarded Member 2');
INSERT INTO Member (id, name) VALUES (7, 'Rewarded Member 3');
INSERT INTO Member (id, name) VALUES (8, 'Rewarded Member 4');

INSERT INTO Reward(id, memberId, rewardType) VALUES (1, 1, "First Reward");
INSERT INTO Reward(id, memberId, rewardType) VALUES (2, 1, "Second Reward");
INSERT INTO Reward(id, memberId, rewardType) VALUES (3, 2, "Third Reward");

INSERT INTO Reward(id, memberId, rewardType) VALUES (4, 6, "First Reward To Delete");
INSERT INTO Reward(id, memberId, rewardType) VALUES (5, 6, "Second Reward To Delete");
INSERT INTO Reward(id, memberId, rewardType) VALUES (6, 7, "Third Reward To Delete");
INSERT INTO Reward(id, memberId, rewardType) VALUES (7, 7, "Third Reward To Delete");
INSERT INTO Reward(id, memberId, rewardType) VALUES (8, 8, "Fourth Reward To Delete");
INSERT INTO Reward(id, memberId, rewardType) VALUES (9, 8, "Fourth Reward To Delete");

-- Down
DELETE FROM Member WHERE id = 1;
DELETE FROM Member WHERE id = 2;
DELETE FROM Member WHERE id = 3;
DELETE FROM Member WHERE id = 4;
DELETE FROM Member WHERE id = 5;
DELETE FROM Member WHERE id = 6;
DELETE FROM Member WHERE id = 7;
DELETE FROM Member WHERE id = 8;
