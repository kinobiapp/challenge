module.exports = {
    members: {
        SELECT_ALL: "SELECT id, name FROM Member",
        SELECT_ONE: "SELECT id, name FROM Member where id = ?",
        INSERT: "INSERT INTO Member(id, name) VALUES (NULL, ?)",
        DELETE: "DELETE FROM Member WHERE id = ?"
    },

    rewards: {
        SELECT_ALL_BY_MEMBER: "SELECT id, memberId, rewardType FROM Reward WHERE memberId = ?",
        SELECT_ONE_BY_MEMBER: "SELECT id, memberId, rewardType FROM Reward WHERE memberID = ? AND id = ?",
        INSERT: "INSERT INTO Reward(id, memberId, rewardType) SELECT NULL, ?, ? WHERE EXISTS (SELECT 1 FROM Member WHERE id = ?)",
        DELETE_ONE_BY_MEMBER: "DELETE FROM Reward WHERE memberId = ? AND id = ?",
        DELETE_BY_MEMBER: "DELETE FROM Reward WHERE memberId = ?"
    }
};
