module.exports = {
    errors: {
        CREATE_MEMBER: 'Create member failed!',
        UNKNOWN_MEMBER: 'Unknown member!',
        CREATE_REWARD: 'Create reward failed!',
        UNKNOWN_REWARD: 'Unknown reward!'
    }
};
