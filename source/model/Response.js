const httpstatus = require('http-status-codes');

module.exports = class Response {
    constructor(status, data) {
        this.status = status;
        this.message = httpstatus.getStatusText(status);
        this.data = data;
    }
};
