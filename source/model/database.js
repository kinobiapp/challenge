var db = require('sqlite');

module.exports = {
    db: db,
    // connect to the database and run migration scripts
    connect: () => {
        return Promise.resolve()
            // Connect to the database
            .then(() => db.open('./database.sqlite', {
                Promise
            }))
            // Migrate its schema to the latest version
            .then(() => db.migrate({
                force: 'last',
                migrationsPath: './source/migrations'
            }));
    }
};
