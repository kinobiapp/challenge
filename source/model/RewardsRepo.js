const {
    db
} = require("./database");

const statements = require('./statements');
const resources = require('./resources');

module.exports = class RewardsRepo {

    static createReward(memberId, reward) {
        return new Promise((resolve, reject) => {
            db.prepare(statements.rewards.INSERT).then(stmt => {
                return stmt
                    .run(memberId, reward.rewardType, memberId)
                    .then((results) => {
                        if (results.stmt.changes != 1) throw new Error(resources.errors.CREATE_REWARD);
                        reward.id = results.stmt.lastID;
                        return resolve(reward);
                    })
                    .catch(err => {
                        return reject(err);
                    });
            });
        });
    }

    static getRewards(memberId) {
        return new Promise((resolve, reject) => {
            db.all(statements.rewards.SELECT_ALL_BY_MEMBER, memberId)
                .then(results => {
                    return resolve(results);
                })
                .catch(err => {
                    return reject(err);
                });
        });
    }

    static getReward(memberId, rewardId) {
        return new Promise((resolve, reject) => {
            db.get(statements.rewards.SELECT_ONE_BY_MEMBER, memberId, rewardId)
                .then(results => {
                    return resolve(results);
                })
                .catch(err => {
                    return reject(err);
                });
        });
    }

    static deleteRewards(memberId) {
        return new Promise((resolve, reject) => {
            db.prepare(statements.rewards.DELETE_BY_MEMBER).then(stmt => {
                return stmt
                    .run(memberId)
                    .then((results) => {
                        if (results.stmt.changes === 0) throw new Error(resources.errors.UNKNOWN_REWARD);
                        return resolve();
                    })
                    .catch(err => {
                        return reject(err);
                    });
            });
        });
    }

    static deleteReward(memberId, rewardId) {
        return new Promise((resolve, reject) => {
            db.prepare(statements.rewards.DELETE_ONE_BY_MEMBER).then(stmt => {
                return stmt
                    .run(memberId, rewardId)
                    .then((results) => {
                        if (results.stmt.changes != 1) throw new Error(resources.errors.UNKNOWN_REWARD);
                        return resolve();
                    })
                    .catch(err => {
                        return reject(err);
                    });
            });
        });
    }
};
