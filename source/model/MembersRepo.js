const {
    db
} = require("./database");

const statements = require('./statements');
const resources = require('./resources');

module.exports = class MembersRepo {
    static readMembers() {
        return new Promise((resolve, reject) => {
            db.all(statements.members.SELECT_ALL)
                .then(results => {
                    return resolve(results);
                })
                .catch(err => {
                    return reject(err);
                });
        });
    }

    static readMember(memberId) {
        return new Promise((resolve, reject) => {
            db.get(statements.members.SELECT_ONE, memberId)
                .then(results => {
                    return resolve(results);
                })
                .catch(err => {
                    return reject(err);
                });
        });
    }

    static createMember(member) {
        return new Promise((resolve, reject) => {
            db.prepare(statements.members.INSERT).then(stmt => {
                return stmt
                    .run(member.name)
                    .then((results) => {
                        if (results.stmt.changes != 1) throw new Error(resources.errors.CREATE_MEMBER);
                        member.id = results.stmt.lastID;
                        return resolve(member);
                    })
                    .catch(err => {
                        return reject(err);
                    });
            });
        });
    }

    static deleteMember(memberId) {
        return new Promise((resolve, reject) => {
            db.prepare(statements.members.DELETE).then(stmt => {
                return stmt
                    .run(memberId)
                    .then((results) => {
                        if (results.stmt.changes != 1) throw new Error(resources.errors.UNKNOWN_MEMBER);
                        return resolve();
                    })
                    .catch(err => {
                        return reject(err);
                    });
            });
        });
    }
};
