module.exports = class Reward {
    constructor(id, memberId, rewardType) {
        this.id = id;
        this.memberId = memberId;
        this.rewardType = rewardType;
    }
};
